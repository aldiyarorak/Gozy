﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq;
using System.Data.Linq.Mapping;


namespace ConsoleApp9
{
    class Program
    {
        static void Main(string[] args)
        {
            Model model = new Model();
            #region First Task
            //List<Orders> orders = model.Orders.ToList();
            //var list = from order in orders
            //           orderby order.OrderDate
            //           select order;
            //Console.WriteLine("Меньшая дата заказа: Id {0} OrderDate {1}", orders[0].OrderID.ToString(), orders[0].OrderDate.ToString());
            //Console.WriteLine("Большая дата заказа: Id {0} OrderDate {1}", orders[orders.Capacity - 1].OrderID.ToString(), orders[orders.Capacity - 1].OrderDate.ToString());
            //Console.ReadLine();
            #endregion
            #region Second Task
            //List<Orders> orders = model.Orders.ToList();
            //var list = from order in orders
            //           orderby order.OrderDate - order.RequiredDate descending
            //           select order;
            //Console.WriteLine("Большая разница в дате заказа и дате нужды: Id {0}", list[0].OrderID.ToString());
            //Console.ReadLine();
            #endregion
            #region Third Task
            //List<Employees> employees = model.Employees.ToList();
            //var list = from employee in employees
            //           where employee.PostalCode.Any(c => char.IsLetter(c))
            //           select employee;

            //foreach (Employees employee in employees)
            //{
            //    Console.WriteLine("Здесь нет букв {0}", employee.EmployeeID.ToString());
            //}
            //Console.ReadLine();
            #endregion
            #region Fourth Task
            //List<Employees> employees = model.Employees.ToList();
            //var list = from employee in employees
            //           orderby employee.Country descending
            //           select employee.Country;
            //IEnumerable<string> listy = list.Distinct();
            //foreach (var employeesCountry in listy)
            //{
            //    Console.WriteLine(employeesCountry);
            //}
            //Console.Read();

            #endregion
            #region Sixth Task
            //List<Order_Details> order_Details = model.Order_Details.ToList();
            //var OrderDetailsList = from orderDetailPrice in order_Details
            //                       group orderDetailPrice by (orderDetailPrice.Quantity * orderDetailPrice.UnitPrice > 5000) ? "More Than 5000$" : (orderDetailPrice.Quantity * orderDetailPrice.UnitPrice < 5000 && orderDetailPrice.Quantity * orderDetailPrice.UnitPrice > 1000) ? "More Than 1000$" : "Less Than 5000$";
            //foreach (var OD in OrderDetailsList)
            //{
            //    Console.WriteLine(OD.Key);
            //    foreach (var inner in OD)
            //    {
            //        Console.WriteLine(inner.OrderID);
            //    }
            //}
            //Console.ReadLine();
            #endregion
            #region Seventh Task
            //List<Order_Details> order_Details = model.Order_Details.ToList();
            //List<Products> products = model.Products.ToList();
            //var OrderDetailsList = from orderDetailPrice in order_Details
            //                       join meta in products on orderDetailPrice.ProductID equals meta.ProductID
            //                       select new { orderDetailPrice.OrderID, meta.ProductName };

            //var grouped = from item in OrderDetailsList
            //              group item by item.OrderID;

            //foreach (var OD in grouped)
            //{
            //    Console.WriteLine(OD.Key);
            //    foreach (var inner in OD)
            //    {
            //        Console.WriteLine(inner.ProductName);
            //    }

            //}
            //Console.ReadLine();
            #endregion
            #region Eighth Task
            //List<Orders> orders = model.Orders.ToList();
            //var list = from order in orders
            //           group order by order.CustomerID;
            //foreach(var customer in list)
            //{
            //    Console.WriteLine(customer.Key);
            //    foreach (var inner in customer)
            //    {
            //        Console.WriteLine(inner.EmployeeID);
            //    }
            //}
            #region Nineth task
            //List<Order_Details> order_Details = model.Order_Details.ToList();
            //List<Products> products = model.Products.ToList();
            //List<Orders> orders = model.Orders.ToList();
            //List<Customers> customers = model.Customers.ToList();
            //var OrderDetailsList = from orderDetail in order_Details
            //                       join meta in products on orderDetail.ProductID equals meta.ProductID
            //                       select new { orderDetail.OrderID, meta.ProductID};
            //var orderList = from order in orders
            //                join gozy in order_Details on order.OrderID equals gozy.OrderID
            //                select new { order.OrderID, order.CustomerID};
            //var OrderXDetailsList = from orderX in orderList
            //                        join gez in OrderDetailsList on orderX.OrderID equals gez.OrderID
            //                        select new { orderX.CustomerID, gez.ProductID };
            //var grouped = from item in OrderXDetailsList
            //              group item by item.ProductID;
            //foreach (var OD in grouped)
            //{
            //    Console.WriteLine(OD.Key);
            //    foreach (var inner in OD)
            //    {
            //        Console.WriteLine(inner.CustomerID);
            //    }
            //}
            #endregion
            #endregion
            #region Twelveth Task
            //List<Orders> orders = model.Orders.ToList();
            //List<string> orderCountries = new List<string>();
            //var list = from order in orders
            //           group order by order.ShipCountry;
            //foreach (var employeesCountry in list)
            //{
            //    orderCountries.Add(employeesCountry.Key);
            //}
            //for(int i = 0; i<3; i++)
            //{
            //    Console.WriteLine(orderCountries[i].ToString()); 
            //}
            //Console.Read();
            #endregion

        }
        public class Customer
        {
            public int CustomerID { get; set; }
            public string CompanyName { get; set; }
            public string ContactName { get; set; }
            public string ContactTitle { get; set; }
            public string Adress { get; set; }
            public string City { get; set; }
            public string Region { get; set; }
            public string PostalCode { get; set; }
            public string Country { get; set; }
            public string Phone { get; set; }
            public string Fax { get; set; }
        }
        public class CustomerConfiguration : EntityTypeConfiguration<Customer>
        {
            public CustomerConfiguration()
            {
                HasKey(p => p.CustomerID);
                Property(p => p.CustomerID).IsRequired();
            }
        }
    }
}
